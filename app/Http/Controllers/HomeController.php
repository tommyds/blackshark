<?php

namespace App\Http\Controllers;

use App\Core\Manager\RedisManager;
use App\Core\Manager\ConnectionManager;
use App\Core\Utils\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redirect;



use Illuminate\Http\Request;

class HomeController extends Controller
{
    //

    public function index(Request $request)
    {
    	# code..
        $dataSession = $request->session()->all();

        // if(count($dataSession) == 1){
        //     return redirect('/');
        // }

        // if($dataSession['session_is'] != true){
        //     return redirect('/');
        // }

    	$get = $request->query->all();

        $refresh = false;

        if (Validator::validate($get, 'refresh', null, 'null')) {
            $refresh = empty($get['refresh']) ? 1 : $get['refresh'] + 1;
        }   

    	$redis = new RedisManager();

    	$searchParam = [
            "refresh" => 1,
            "limit" => 3,
        ];

        $data["news"] = $redis->get('news.black-shark');
        if (empty($data["news"]) || $refresh) {
        	$connectionManager = new ConnectionManager(env('API_DS'));
        	$out = $connectionManager->stream('/tag/black-shark', 'GET', $searchParam, array(), true);
        	$data["news"] = $out["dt"]["news"];

        	$redis->set('news.black-shark', $data["news"], 60 * 60 * 2);
        }
        // dd($data);

    	return view('layouts.app',$data);
    }

    public function login(Request $request)
    {
        # code...
        $request->session()->put('session_is', false);
        return view('page.login');
    }

    public function loginAction(Request $request)
    {
        # code...
        $username = "admin";
        $password = "DSxBS";

        $user =  $request->request->all();

        if($user['username'] == $username && $user['pass'] == $password){
            $request->session()->put('session_is', true);
            return redirect('home');
        }else{
            $request->session()->put('session_is', false);
            Session::flash('error','Login Failed!');
            return redirect('/');
        }
    }
}
