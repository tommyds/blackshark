<div id="home" class="home">
	<div class="header">
		<div class="row">
			<div class="menu-header">
				<ul>
					<!-- <li class="list-menu"><a class="scrollMenu" href="#home"> Beranda</a></li> -->
					<li class="list-menu"><a class="scrollMenu" href="#contact"> S&K</a></li>
					<li class="list-menu"><a class="scrollMenu" href="#about_us"> Tentang</a></li>
					<li class="list-menu"><a class="scrollMenu" href="#event"> Acara</a></li>
					<li class="list-menu logo"><a class="scrollMenu" href="#home"><img src="{{asset('../images/Group59.svg')}}" width="100" height="100"></a></li>
					<li class="list-menu"><a class="scrollMenu" href="#news"> Berita</a></li>
					<li class="list-menu"><a class="scrollMenu" href="#footer"> Hubungi kami</a></li>
				</ul>
			</div>	
		</div>
	</div>
	<div class="logo-mobile">
		<img src="{{asset('../images/Group59.svg')}}">
	</div>
	<div class="container h-100vh">
		<div class="row h-85 align-items-center">
			<div class="col-md-12">
				<div class="home__content">
					<div data-aos="fade-up">
						<p class="team">
							JADI PEMENANG
						</p>
						<p class="reward">
							DAPATKAN 8.000.000 RUPIAH<br>TOTAL HADIAH
						</p>
						<!-- <p class="month">
							SETIAP BULAN
						</p> -->
						<p class="desc_home col-lg-6 offset-lg-3">
							Apakah anda adalah pemain terbaik Mobile Legends: Bang Bang? Gabung dengan kompetisi rookie kami dan bawa pulang 8 juta rupiah, smartphone Black Shark untuk hadiah turnamen serta merchandise Black Shark untuk penonton.
						</p><br>
						<p class="hashtag">#blacksharkclan #blacksharkcommunity</p>
					</div>
					<div data-aos="fade-down">
						<a href="#live" type="button" class="btn btn-join btn-lg">Livestream</a>
					</div>
				</div>
					
			</div>
		</div>
	</div>
	<div class="logo-footer-home">
		<div class="row">
			<div class="support col-md-6">Supported By:</div>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12"><a href="https://global.blackshark.com/" style="margin-right: 5px"><img src="{{asset('../images/logoshark1.png')}}" width="145" height="38"></a><a href="http://hybrid.co.id" style="margin-right: 5px"><img src="{{asset('../images/WHITE.png')}}"></a><a href="http://bit.ly/392sVN9"><img src="{{asset('../images/erafon.png')}}" width="145" height="25" style="margin-bottom: 7px;"></a></div>
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="share">
	<div class="popup share-btn"><span id="share-event">Bagikan Acara</span>
		<span class="popuptext" id="myPopup">
			<div class="btn-pos">
				<a href="https://www.facebook.com/dialog/share?app_id=41444726519&display=popup&href=https://blackshark.hybrid.co.id&quote=Join%20Black%20Shark%20Clan%20for%20mobile%20gamers.%20%23blacksharkclan%20%23blacksharkcommunity%20https%3A%2F%2Fblackshark.hybrid.co.id" class="btn btn-socmed"><img src="{{asset('../images/facebook.svg')}}" width="40"></a><br>
				<!-- <a href="https://twitter.com/intent/tweet?text=Join%20Black%20Shark%20Clan%20for%20mobile%20gamers.%20%23blacksharkclan%20%23blacksharkcommunity%20https%3A%2F%2Fblackshark.hybrid.co.id" class="btn btn-socmed"><img src="{{asset('../images/twitter.svg')}}" width="40"></a><br> -->
				<a href="https://www.instagram.com/blackshark.idn/" class="btn btn-socmed"><img src="{{asset('../images/instagram.svg')}}" width="40"></a>
			</div>
		</span>
	</div>
</div>