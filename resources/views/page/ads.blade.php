<div id="ads" class="ads">
	<div class="video-wording"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/h3bndpkYZUE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>
		<!-- <iframe src="https://drive.google.com/file/d/1gjSpcpwAdUf0hrl4zlWogNHoFxeJ-pwO/preview" width="100%" height="100%"></iframe>--></div> 
	<div class="wording">
		<div class="word-desc">
			Experience the best in gaming with Black Shark!
			Unlock more Black Shark product <a href="http://bit.ly/392sVN9">here</a>.
		</div>
	</div>
</div>
<div id="streaming" class="streaming">
	
</div>
<div id="news" class="news">
	<div class="news-community">
		<p class="title-news-category">
			Berita Terkait
		</p>
		@foreach($news['posts'] as $key => $item)
			<div class="hybrid-news" data-aos="fade-up">
				<div class="row">
					<div class="col-lg-3 img-news">
						<img src="{{$item['thumbnail']}}" width="361px" height="203px;">
					</div>
					<a href="{{$item['url']}}">
						<div class="col-md-8">
							<!-- <div class="from-news">Hybrid.co.id</div> -->
							<div class="title-news">{{$item['title']}}</div>
							<div class="date-news">{{ Carbon\Carbon::parse($item['date'])->format('d M Y') }}</div>
						</div>
					</a>
				</div>
			</div>
		@endforeach
	</div>
</div>