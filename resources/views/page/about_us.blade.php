<div id="about_us" class="about_us">
	<div class="container">
<!-- 		<div class="row">
			<div class="col-md-12"> -->
				<div class="about__content">
					<div data-aos="fade-down">
						<div class="about__content-title">
							<p class="whatis">
								Apa itu
							</p>
							<p class="clan">
								Black Shark Clan?
							</p>
						</div>
						<p class="desc_about">
							Black Shark Clan adalah komunitas mobile gaming dari Black Shark, yang diperuntukkan bagi para pemain rookie untuk meningkatkan kemampuan mereka. Kompetisi yang akan diadakan terbuka bagi para pemain Mobile Legends: Bang Bang (MLBB).<br><br><font class="hashtag">#blacksharkclan #blacksharkcommunity</font>
						</p>
						<div class="community_game">
							<p class="community">Mini Tournament:</p>
							<img src="{{asset('../images/mobile-legend.png')}}">
						</div>
					</div>
					<div style="clear:both; float:none;"></div>
				</div>
			<!-- </div>
		</div> -->
	</div>
</div>