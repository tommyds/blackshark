<div id="event" class="event">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="event__content">
					<div data-aos="zoom-in">
						<p class="title_event">
							Blackshark
						</p>
						<p class="casual">
							Casual Meetup
						</p>
						<p class="desc_meetup col-6 offset-3">
							Black Shark Casual Meetup is an offline meetup where mobile gamers gather, practice, spar, and even meet with the gaming influencers. This is your dojo for preparing yourself to the Community Tournament. There will also be a mini tournament here for qualification phase of Community Tournament.
						</p>

						<button type="button" class="btn btn-join btn-lg" data-toggle="modal" data-target="#join-meetup">Join Meetup</button>
					</div>
				</div>
					
			</div>
		</div>
		<div id="tnc" class="row">
			<div class="col-md-12">
				<div class="tournament__content">
					<div data-aos="zoom-in">
						<p class="title_tournament">
							Blackshark 
						</p>
						<p class="community_tournament">
							Community Tournament
						</p>
						<p class="desc_tournament col-6 offset-3">
							Black Shark Community Tournament is a rookie tournament for rising stars on esports scene. We gather 64 teams for each competition (MLBB, CODM, and PUBGM) on a single elimination tournament.
						</p>
						<button type="button" class="btn btn-join-green btn-lg" data-toggle="modal" data-target="#comm_tournament">Coming Soon</button>
					</div>
				</div>
				
			</div>
		</div>
		<div data-aos="zoom-in">
			<div class="row rules">
				<div class="col-md-4">
					<div class="mobile-legend">
						<img class="ml" src="{{asset('../images/mobile-legend.png')}}">
						<button type="button" class="btn btn-rules-green btn-lg" data-toggle="modal" data-target="#mobile-legend-game">See Rules</button>
					</div>
				</div>
				<div class="col-md-4">
					<div class="battleground">
						<img src="{{asset('../images/logo_PUBG-1.png')}}" width="248px">
						<a href="#tnc" type="button" class="btn btn-rules-green btn-lg">Coming Soon</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="cod">
						<img src="{{asset('../images/cod.png')}}" width="267px">
						<a href="#tnc" type="button" class="btn btn-rules-green btn-lg">Coming Soon</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="mobile-legend-game" tabindex="-1" role="dialog" aria-labelledby="mobile-legend-game-tittle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content modal-rules">
      <div class="modal-header">
        <h5 class="modal-title rules-title" id="battle-tittle">Rules Mobile Legend</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-rules">
       		<strong>Registration Rules - <a href="#contact" data-dismiss="modal"> see rules</a> 
			<br>
			Competition Rules:
			</strong><br><br>
			<strong>1. Peraturan Umum</strong>

			<p><strong>1.1 Peserta</strong></p>

			<p><span style="font-weight: 400;">- Peserta adalah individu perorangan yang telah mendaftar dalam form registrasi <a href="http://bit.ly/BSclan">(link pendaftaran)</a></span></p>

			<p><span style="font-weight: 400;">- Setelah peraturan ini dikirimkan pada peserta maka panitia tidak lagi menerima perubahaan untuk peserta yang telah terdaftar</span></p>

			<p><span style="font-weight: 400;">- Peserta tidak boleh diwakilkan oleh siapapun untuk mengikuti turnamen ini apapun alasannya. Jika melanggar maka akan dianggap tidak hadir</span></p>

			<p><span style="font-weight: 400;">- Peserta harus bersedia dan menerima jam dan tanggal main yang sudah ditentukan oleh panitia</span></p>

			<p><span style="font-weight: 400;">- Untuk pertandingan offline, panitia tidak menyediakan akomodasi</span></p>

			<p><span style="font-weight: 400;">- Peserta wajib untuk mengikuti semua peraturan yang disediakan oleh panitia</span></p>

			<p><span style="font-weight: 400;">- Panitia berhak mengubah peraturan dan memutuskan sesuatu di luar peraturan yang ada</span></p>

			<p><span style="font-weight: 400;">- Dengan mendaftar para peserta dianggap mengerti, paham dan bersedia untuk mematuhi semua peraturan yang ada</span></p>

			<p><span style="font-weight: 400;">- Setiap tim wajib memiliki minimal 5 pemain utama ( boleh memiliki 1 cadangan)</span></p>

			<p><span style="font-weight: 400;">- Minimum rank: EPIC</span></p>

			<p><span style="font-weight: 400;">- Tidak diperbolehkan MULTISLOT atau MULTITEAM.</span></p>
			<p><br /><br /></p>
			<p><strong><em>1.2 Fair Play</em></strong></p>

			<p><span style="font-weight: 400;">- Fair play sangat dianjurkan dan dijunjung tinggi demi terciptanya turnamen yang kondusif</span></p>

			<p><span style="font-weight: 400;">- Dilarang menggunakan program ilegal, bug, match fixing, dan tindak kecurangan lainnya</span></p>

			<p><strong><em>1.3 Diskualifikasi dan WO</em></strong></p>

			<p><span style="font-weight: 400;">- Peserta yang tidak bisa hadir setelah 15 menit dari jadwal yang sudah ditentukan akan dianggap gugur atau Walk Out</span></p>

			<p><span style="font-weight: 400;">- Peserta yang mencoba atau melakukan kecurangan dan melanggar peraturan dalam bentuk apapun akan didiskualifikasi</span></p>

			<p><span style="font-weight: 400;">- Peserta yang dengan sengaja melakukan toxic, flaming, menyinggung ras, agama, etnis dan hal pribadi lainnya akan kami diskualifikasi</span></p>

			<p><span style="font-weight: 400;">- Peserta yang dengan sengaja melakukan/menggunakan bug, glitch, atau error pada game yang menguntungkan pengguna akan kami diskualifikasi</span></p>
			<p><strong><em><br /></em></strong><strong><em>1.4 Sebelum Pertandingan</em></strong></p>

			<p><span style="font-weight: 400;">- Seluruh peserta diwajibkan siap 30 menit sebelum pertandingan dimulai.</span></p>

			<p><span style="font-weight: 400;">- Peserta wajib hadir sesuai jadwal yang ditentukan dan sudah siap untuk bertanding sesuai dengan jam yang ditentukan juga</span></p>

			<p><span style="font-weight: 400;">- Sebelum memulai turnamen, peserta wajib absen/check in ke panitia</span></p>

			<p><span style="font-weight: 400;">- Jika setelah 15 menit terlambat dari jadwal yang sudah tertera maka akan kami anggap tidak hadir.</span></p>

			<p><strong><em>1.5 Saat Pertandingan</em></strong></p>

			<p><span style="font-weight: 400;">- Koneksi yang menggunakan layanan internet pribadi peserta yang bermasalah di luar tanggung jawab panitia dan menjadi tanggung jawab masing masing peserta.&nbsp;</span></p>
			<p><span style="font-weight: 400;">- Kedua tim melakukan kesepakatan waktu tanding sesuai dengan waktu yang telah dianjurkan (untuk online qualifier).&nbsp;</span></p>

			<p><span style="font-weight: 400;">- Apabila tidak ada kesepakatan waktu, maka match harus dilangsungkan pada Default Time yang telah tertera.&nbsp;</span></p>

			<p><span style="font-weight: 400;">- Toleransi keterlambatan dari kesepakatan waktu tanding maupun default time adalah 15 menit.&nbsp;</span></p>

			<p><span style="font-weight: 400;">- Seluruh peserta menggunakan akun ID masing-masing. Keterlambatan sangat tidak ditoleransi oleh pihak penyelenggara, dan menyebabkan diskualifikasi.&nbsp;</span></p>

			<p><span style="font-weight: 400;">- Apabila sudah memasuki waktu pertandingan (yang telah ditentukan dan diberitahukan sebelumnya), namun anggota dari salah satu tim/kedua tim masih belum lengkap (kurang dari 5 orang), maka pertandingan akan tetap dilanjutkan.&nbsp;</span></p>

			<p><span style="font-weight: 400;">- Jumlah minimum anggota tim agar pertandingan bisa dimulai adalah 4 orang (Jumlah minimum masing-masing tim), apabila ada salah satu tim yang jumlahnya kurang dari 4 orang, maka tim tersebut akan dianggap kalah pada pertandingan tersebut.&nbsp;</span></p>

			<p><span style="font-weight: 400;">- Kesalahan/kelalaian yang terjadi saat pertandingan, yang diakibatkan pribadi dari player/tim bukan tanggung jawab penyelenggara, dan pertandingan akan tetap dilanjutkan.&nbsp;</span></p>

			<p><span style="font-weight: 400;">- Batas maksimal pause adalah 3 menit untuk masing-masing tim pertandingan wajib dilanjutkan kembali apabila melebihi batas maksimal pause.&nbsp;</span></p>

			<p><span style="font-weight: 400;">- Segala bentuk cheat, hack, dan eksploitas tidak ditolerir, apabila salah satu tim melanggar maka game akan segera dihentikan dan tim yang melanggar didiskualifikasi.&nbsp;</span></p>

			<p><span style="font-weight: 400;">- Pemenang wajib melakukan screenshot scoreboard akhir dan melaporkan di group WA, jika tidak ada screenshot yang dilaporkan, maka kedua tim di anggap gugur di match tersebut, dan tidak ada pemenang yang melaju ke babak selanjutnya.</span><span style="font-weight: 400;"><br /><br /></span></p>
			<p><span style="font-weight: 400;">- Dilarang keras melakukan tindakan toxic/flaming/kecurangan secara sengaja maupun tidak disengaja dalam bentuk apapun selama pertandingan berlangsung. Peserta yang melakukan hal-hal di atas akan terkena diskualifikasi oleh panitia.</span></p>

			<p><span style="font-weight: 400;">- Jika pertandingan mengalami error, bug atau glitch harap melaporkan hal tersebut ke panitia terlebih dahulu. Peserta dilarang memutuskan tindak lanjut terkait masalah di atas. Hanya panitia yang berhak memutuskan.</span></p>

			<p><span style="font-weight: 400;">- Permainan akan diulang jika salah satu pemain mengalami error/DC&nbsp; setelah 5 menit pertandingan berlangsung jika tidak ada damage yang terjadi.</span><span style="font-weight: 400;"><br /><br /></span></p>

			
			<strong>2. Tanggal dan tempat</strong>

			<p><span style="font-weight: 400;">Casual tournament - Black Shark Clan @Hybrid Dojo Jl. Kemang Selatan 1D No. 2</span></p>
			<p><span style="font-weight: 400;">Online Qualifier TBD</span></p>
			<p><span style="font-weight: 400;">Offline Tournament - Black Shark Clan @Hybrid Dojo Jl. Kemang Selatan 1D No. 2</span></p>
			<br>
			
			<strong>3. Hadiah</strong><strong><br /></strong><span style="font-weight: 400;">Casual tournament Rp1.000.000</span>
			<p><span style="font-weight: 400;">Offline tournament Rp.10.000.000</span></p>
			<br>
			<strong>4. Format Turnamen</strong>

			<p><span style="font-weight: 400;">Mode: Custom - Draft pick 5 v 5</span></p>

			<p><span style="font-weight: 400;">Casual tournament&nbsp;</span></p>
			<p><span style="font-weight: 400;">Sistem pertandingan: Double Elimination BO 1</span></p>

			<p><span style="font-weight: 400;">Online qualifier: Pool - BO 1</span></p>

			<p><span style="font-weight: 400;">Offline tournament</span></p>
			<p><span style="font-weight: 400;">Sistem pertandingan: Double Elimination BO 1, Final BO 3.&nbsp;</span></p>
			<p><br /><br /></p>
			<p><span style="font-weight: 400;">Skin: ON</span></p>
			<p><span style="font-weight: 400;">Penentuan pick: Toss coin untuk menentukan first pick/first ban.</span></p>
			<p><span style="font-weight: 400;">Kondisi menang: - Salah satu dari base kedua tim hancur.</span></p>
			<p><span style="font-weight: 400;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Salah satu tim menyatakan surrender.</span></p>

			<p><span style="font-weight: 400;">Hero yang dilarang digunakan: Hero yang baru dirilis kurang dalam waktu 2 minggu tidak boleh digunakan.</span></p>
			<p><br /><br /><br /></p>
			<strong>5. Komplain</strong>
			<ol>
			<li style="font-weight: 400;"><span style="font-weight: 400;">Lingkupan komplain : segala situasi yang mempengaruhi keadilan dalam permainan.</span><span style="font-weight: 400;"><br /><br /></span></li>
			<li style="font-weight: 400;"><span style="font-weight: 400;">Syarat komplain yang harus dipenuhi :</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Materi yang disiapkan team yang komplain setidaknya satu dari video, screenshot dalam permainan, screenshot riwayat chat, foto dan materi lainnya, tidak terbatas pada materi di atas saja. Menjelaskan tim/pemain mana yang dikomplain, menyampaikan detail dan analisa, serta bersedia membantu panitia saat diperlukan untuk menentukan apa komplain tersebut benar melanggar peraturan.</span><span style="font-weight: 400;"><br /><br /></span></li>
			<li style="font-weight: 400;"><span style="font-weight: 400;">Periode complain:</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Komplain maksimal diajukan dalam 3 jam setelah pertandingan berakhir, jika periode lewat maka komplain tidak diterima.</span><span style="font-weight: 400;"><br /><br /></span></li>
			<li style="font-weight: 400;"><span style="font-weight: 400;">Cara penyelesaian</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Melalui analisa bukti (pemutaran ulang rekaman, pertimbangan screenshot, dll), jika situasi menunjukkan bahwa ada kecurangan, maka akan ditindak sesuai dengan hukum yang berlaku dan akan dipublikasikan; Hasil pertandingan akan dikoreksi sesuai aturan, jika tidak bisa dikoreksi akan dipertimbangkan tanding ulang.</span></li>
			</ol>

			<strong>6. Force Majuere</strong><strong><br /></strong><span style="font-weight: 400;"><br /></span><strong><em>Force majeure</em></strong><span style="font-weight: 400;"> yang berarti "kekuatan yang lebih besar" adalah suatu kejadian yang terjadi di luar kemampuan </span><span style="font-weight: 400;">manusia</span><span style="font-weight: 400;"> dan tidak dapat dihindarkan sehingga suatu kegiatan tidak dapat dilaksanakan atau tidak dapat dilaksanakan sebagaimana mestinya.</span>
			<p><span style="font-weight: 400;">Yang termasuk kategori keadaan kahar adalah </span><span style="font-weight: 400;">peperangan</span><span style="font-weight: 400;">, </span><span style="font-weight: 400;">kerusuhan</span><span style="font-weight: 400;">, </span><span style="font-weight: 400;">revolusi</span></a><span style="font-weight: 400;">, </span><span style="font-weight: 400;">bencana alam</span><span style="font-weight: 400;">, </span><span style="font-weight: 400;">pemogokan</span><span style="font-weight: 400;">, </span><span style="font-weight: 400;">kebakaran</span><span style="font-weight: 400;">, dan bencana lainnya yang harus dinyatakan oleh pejabat/instansi yang berwenang.</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Panitia tidak bertanggung jawab jika salah satu dari tim terdampak kerugian dari contoh-contoh kejadian di atas seperti masalah koneksi para pemain juga termasuk.</span></p>


			<strong> Lainnya</strong>

			<p><span style="font-weight: 400;">Panitia berhak menambahkan, mengubah, peraturan yang ada demi menjaga ketertiban berlangsungnya turnamen. Semua keputusan dari panitia yang bertugas adalah mutlak dan tidak bisa diganggu gugat.</span></p>
			<br>
			<a href="http://bit.ly/BSclan" type="button" class="btn btn-join btn-lg">Join Now</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success close" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="battle" tabindex="-1" role="dialog" aria-labelledby="battle-tittle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content modal-rules">
      <div class="modal-header">
        <h5 class="modal-title rules-title" id="battle-tittle">Rules Battleground</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-rules">
    	Coming Soon
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success close" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="cod-game" tabindex="-1" role="dialog" aria-labelledby="cod-game-tittle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content modal-rules">
      <div class="modal-header">
        <h5 class="modal-title rules-title" id="battle-tittle">Rules Call Of Duty</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-rules">
       	Coming Soon
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success close" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="comm_tournament" tabindex="-1" role="dialog" aria-labelledby="comm_tournament" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content modal-rules">
      <div class="modal-header">
        <h5 class="modal-title rules-title" id="battle-tittle">Community Tournament</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-rules">
       	Coming Soon
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success close" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="join-meetup" tabindex="-1" role="dialog" aria-labelledby="join-meetup" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content modal-rules">
      <div class="modal-header">
        <h5 class="modal-title rules-title" id="battle-tittle">Join Meetup</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-rules">
       	<p><span style="font-weight: 400;">Black Shark Clan Casual Meetup is an offline meetup where mobile gamers gather, practice, spar, and even meet with the gaming influencers.&nbsp;</span></p><br>
		<p><span style="font-weight: 400;">This is your dojo for preparing yourself for the Community Tournament. There will also be a mini tournament here for the qualification phase of the Community Tournament</span><span style="font-weight: 400;">.</span></p><br>
		<p><span style="font-weight: 400;">Mini tournament only for the players that come to gathering. There will be a Rp1.000.000 total prize for the mini tournament at the gathering. You also can try the latest device provided by Black Shark.</span></p><br>
		<p><span style="font-weight: 400;">Special merchandise in the goodie bag and light meals are also available at the gathering.&nbsp;</span></p>
		<p><span style="font-weight: 400;">Registration process will start from 8 March - 3 April 2020 online on this link. </span><span style="font-weight: 400;">This is a free event.&nbsp;</span></p>
		<p><span style="font-weight: 400;">Casual meetup for Black Shark Clan 11 April 2020, start from 13.00 PM - finish.</span></p>
		<p><span style="font-weight: 400;">Black Shark clan will be held in Hybrid Dojo @HybridIDN HQ Jl. Kemang Selatan 1D No.2</span></p><br>
		<p><span style="font-weight: 400;">Don't forget to read the requirement on this link. Follow the instructions, submit your answer and wait. We will call you if you are eligible for the event.&nbsp;</span></p><br><br>
		<p><span style="font-weight: 400;">Goodluck!</span></p>
		<p><span style="font-weight: 400;">Mini tournament guidelines: <a href="#contact" data-dismiss="modal">see rules</a></span></p>
		<p><br /><span style="font-weight: 400;">Contact person: Fauzan (0822-1759-9096) email: </span><a href="mailto:fauzhan@hybrid.co.id"><span style="font-weight: 400;">fauzhan@hybrid.co.id</span></a><span style="font-weight: 400;"> or </span><a href="mailto:admin@hybrid.co.id"><span style="font-weight: 400;">admin@hybrid.co.id</span></a><span style="font-weight: 400;">.</span></p>
       	<a href="http://bit.ly/BSclan" type="button" class="btn btn-join btn-lg">Join Meetup</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success close" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>