<div id="contact" class="contact">
	<div class="bg-dots"></div>
	<div class="figure"></div>
	<div class="contact__content">
		<div class="contact-explain">
			<div data-aos="zoom-in">
				<p class="title-exp">Black Shark Clan Meetup and Casual Tournament</p>
				<p class="how-to">
					1. Fill registration form this <a href="http://bit.ly/BSclan">link</a>, follow requirement given.<br>
					2. Follow @blackshark.idn on Instagram, Facebook and Twitter.<br>
					3. <a href="{{asset('../images/Border.png')}}" download>Download</a> this template to your profile picture on social media.<br>
					4. Share about this event to your friend on social media as much as possible.<br>
				</p>
				<button type="button" class="btn btn-rules-purple btn-lg" data-toggle="modal" data-target="#qualifier-rule">See Rules</button>
				<!-- <p class="how-to-follow">
					1. Submit your statistic match <br>
					2. Follow @blackshark.idn on Instagram
				</p> -->
				<div class="row qualifier">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">
								<p class="btn-purple-position">
									<p class="qualifier">Online Qualifier</p>
									<p class="qualifier-soon"><a href="#tnc">Coming Soon</a></p>
								</p>
							</div>
							<div class="col-md-6">
								<div class="logo-shark"><img src="{{asset('../images/Blackshark.png')}}"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="qualifier-rule" tabindex="-1" role="dialog" aria-labelledby="qualifier-rule-tittle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content modal-rules modal-rules-qualifier">
      <div class="modal-header">
        <h5 class="modal-title qualifier-rules-title" id="battle-tittle">Rules Of Qualifier</h5>
        <button type="button" class="close close-qualifier" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-rules-qualifier">
        <p><span style="font-weight: 400;">Only selected tim are eligible to join Black Shark Clan community gathering. Follow the instructions below. Only your captain fill the form but make sure every member of your time shares this event with their social media as much as possible.&nbsp;</span></p><br>
		<p><span style="font-weight: 400;">The eligible team that will be chosen will be seen from their answer form the form and also share in the social media. Make sure you follow every process. This is a free event.</span></p><br>
		<ol>
		<li><span style="font-weight: 400;"> Fill registration form this <a href="http://bit.ly/BSclan">link</a>, follow the requirements given. Answer every question.&nbsp;</span></li>
		<li><span style="font-weight: 400;"> Follow @blackshark.idn on Instagram, Facebook and Twitter.</span></li>
		<li><span style="font-weight: 400;"> <a href="{{asset('../images/Border.png')}}" download>Download</a> this template to your profile picture on social media.</span></li>
		<li><span style="font-weight: 400;"> Share about this event to your friend on social media as much as possible.</span></li>
		</ol>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success close-btn" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>