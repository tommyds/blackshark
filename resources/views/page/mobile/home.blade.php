<div id="home" class="home">
	<div class="header">
		<div class="row">
			<div class="menu-header">
				<ul>
					<li class="list-menu"><a class="scrollMenu" href="#home"> Home</a></li>
					<li class="list-menu"><a class="scrollMenu" href="#event"> Event</a></li>
					<li class="list-menu"><a class="scrollMenu" href="#about_us"> About</a></li>
					<li class="list-menu logo"><a class="scrollMenu" href="#home"><img src="{{asset('../images/Group12.png')}}"></a></li>
					<li class="list-menu"><a class="scrollMenu" href="#contact"> Terms & Conditions</a></li>
					<li class="list-menu"><a class="scrollMenu" href="#footer"> Contact</a></li>
				</ul>
			</div>	
		</div>
	</div>
	<div class="container h-100vh">
		<div class="row h-85 align-items-center">
			<div class="col-md-12">
				<div class="home__content">
					<div data-aos="fade-up">
						<p class="team">
							BEAT 63 TEAM.
						</p>
						<p class="reward">
							WIN IDR 10.000.000
						</p>
						<p class="month">
							EVERY MONTH.
						</p>
						<p class="desc_home col-6 offset-3">
							Are you the best at Mobile Legends: Bang Bang, Call of Duty Mobile, or PUBG Mobile? Join our rookie competition and bring home 10,000,000 for every game tournament!
						</p>
						<p class="hashtag">#blacksharkclan #blacksharkcommunity</p>
					</div>
					<div data-aos="fade-down">
						<a href="http://bit.ly/BSclan" type="button" class="btn btn-join btn-lg">Join Now</a>
					</div>
				</div>
					
			</div>
		</div>
	</div>
	<div class="logo-footer-home">
		<div class="row">
			<div class="support col-md-6">Supported By:</div>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12"><a href="https://global.blackshark.com/"><img src="{{asset('../images/Group17.png')}}"></a><a href="http://hybrid.co.id"><img src="{{asset('../images/WHITE.png')}}"></a></div>
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="share">
	<div class="popup share-btn"><span>Share this event</span>
		<span class="popuptext" id="myPopup">
			<div class="btn-pos">
				<a href="https://www.facebook.com/dialog/share?app_id=41444726519&display=popup&href=https://blackshark.hybrid.co.id&quote=Join%20Black%20Shark%20Clan%20for%20mobile%20gamers.%20%23blacksharkclan%20%23blacksharkcommunity%20https%3A%2F%2Fblackshark.hybrid.co.id" class="btn btn-socmed"><img src="{{asset('../images/facebook.svg')}}" width="40"></a><br>
				<a href="https://twitter.com/intent/tweet?text=Join%20Black%20Shark%20Clan%20for%20mobile%20gamers.%20%23blacksharkclan%20%23blacksharkcommunity%20https%3A%2F%2Fblackshark.hybrid.co.id" class="btn btn-socmed"><img src="{{asset('../images/twitter.svg')}}" width="40"></a><br>
				<a href="#" class="btn btn-socmed"><img src="{{asset('../images/instagram.svg')}}" width="40"></a>
			</div>
		</span>
	</div>
</div>