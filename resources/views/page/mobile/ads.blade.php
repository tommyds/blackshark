<div id="ads" class="ads">
	<div class="video-wording"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/XPT9LEeizlA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
	<div class="wording">
		<div class="word-desc">
			Experience the best in gaming with Black Shark!
			Unlock more Black Shark product <a href="https://global.blackshark.com/">here</a>.
		</div>
	</div>
</div>
<div id="streaming" class="streaming">
	
</div>
<div id="news" class="news">
	<div class="news-community">
		<p class="title-news-category">
			News on Communities
		</p>
		@foreach($news['posts'] as $key => $item)
			<div class="hybrid-news" data-aos="fade-up">
				<div class="row">
					<div class="col-md-3 img-news">
						<img src="{{$item['thumbnail']}}" width="361px" height="203px;">
					</div>
					<a href="{{$item['url']}}">
						<div class="col-md-8">
							<!-- <div class="from-news">Hybrid.co.id</div> -->
							<div class="title-news">{{$item['title']}}</div>
							<div class="date-news">{{ Carbon\Carbon::parse($item['date'])->format('d M Y') }}</div>
						</div>
					</a>
				</div>
			</div>
		@endforeach
	</div>
</div>