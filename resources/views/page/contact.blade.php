<div id="contact" class="contact">
	<div class="bg-dots"></div>
	<div class="figure"></div>
	<div class="contact__content">
		<div class="contact-explain">
			<div data-aos="zoom-in">
				
				<p class="how-to"><b>Anda bisa bergabung di Black Shark Clan lewat: Online Mini Tournament</b></p> 
				<p class="how-to"><b>Pendaftaran dibuka dari 24 Juli 2020 s/d 21 Agustus 2020</b></p>
				<p class="title-exp">Mini Tournament</p>
				<p class="how-to">
					<!-- 1. Fill registration form this <a href="http://bit.ly/BSclan">link</a>, follow requirement given.<br>
					2. Follow @blackshark.idn on Instagram, Facebook and Twitter.<br>
					3. <a href="{{asset('../images/Border.png')}}" download>Download</a> this template to your profile picture on social media.<br>
					4. Share about this event to your friend on social media as much as possible.<br> -->
					1. Isi secara lengkap form registrasi di tautan <a href="#" title="Registrasi Di Tutup">ini</a>, ikuti secara detail persyaratan yang diminta.<br> 
					2. Follow akun @blackshark.idn di Instagram, Facebook dan Twitter.<br>
					3. <a href="{{asset('../images/Border.png')}}" download>Unduh</a> template ini dan tempelkan di profil picture kamu di media sosial. <br>
					4. Share tentang acara ini ke teman Anda lewat media sosial sebanyak mungkin.<br>
				</p>
				<button type="button" class="btn btn-rules-purple btn-lg" data-toggle="modal" data-target="#qualifier-rule">Lihat Aturan</button>
				
				<!-- <div class="row qualifier">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">
								<p class="btn-purple-position">
									<p class="qualifier">Online Qualifier</p>
									<p class="how-to-follow">
										1. Submit your statistic match<br>
										2. Follow @blackshark.idn on Instagram<br>
									</p>
									<a href="#tnc" class="btn btn-rules-purple btn-lg">Cek Disini</a>
								</p>
							</div>
							<div class="col-md-6">
								<div class="logo-shark"><img src="{{asset('../images/Group59.svg')}}"></div>
							</div>
						</div>
					</div>
				</div> -->
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="qualifier-rule" tabindex="-1" role="dialog" aria-labelledby="qualifier-rule-tittle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content modal-rules modal-rules-qualifier">
      <div class="modal-header">
        <h5 class="modal-title qualifier-rules-title" id="battle-tittle">Aturan Qualifier</h5>
        <button type="button" class="close close-qualifier" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-rules-qualifier">
        <!-- <p><span style="font-weight: 400;">Only selected tim are eligible to join Black Shark Clan community gathering. Follow the instructions below. Only your captain fill the form but make sure every member of your time shares this event with their social media as much as possible.&nbsp;</span></p><br>
		<p><span style="font-weight: 400;">The eligible team that will be chosen will be seen from their answer form the form and also share in the social media. Make sure you follow every process. This is a free event.</span></p><br>
		<ol>
		<li><span style="font-weight: 400;"> Fill registration form this <a href="http://bit.ly/BSclan">link</a>, follow the requirements given. Answer every question.&nbsp;</span></li>
		<li><span style="font-weight: 400;"> Follow @blackshark.idn on Instagram, Facebook and Twitter.</span></li>
		<li><span style="font-weight: 400;"> <a href="{{asset('../images/Border.png')}}" download>Download</a> this template to your profile picture on social media.</span></li>
		<li><span style="font-weight: 400;"> Share about this event to your friend on social media as much as possible.</span></li>
		</ol> -->

		<p align="left">Hanya peserta terpilih yang bisa bergabung dengan komuntias elite Black Shark Clan. Pendaftaran dibuka dari 24 Juli 2020 s/d 21 Agustus 2020.</p>
		<p align="left">&nbsp;</p>
		<p align="left">Ikuti petunjuk di <a href="http://bit.ly/BSclan">tautan ini</a>.</p><br>
		<p align="left">Hanya kapten tim yang mengisi formulir yang disediakan dan pastikan setiap member dari tim membagikan info acara ini (lewat button shares di situs) ke media sosial.</p><br>
		<p align="left">Tim yang terpilih akan didasarkan pada jawaban paling menarik yang diberikan dari pertanyaan di form pendaftaran serta seberapa sering mereka membagikan acara ini di media sosial. Isi formulir di tautan ini, ikuti petunjuk yang ada.</p><br>
		<p align="left">Jawab pertanyaan dengan lengkap. Ikuti akun @blackshark.idn di Instagram, Facebook dan Twitter. </p><br>
		<p align="left"><a href="{{asset('../images/Border.png')}}" download>Unduh</a> template ini dan tempelkan di profil picture media sosial kamu. Bagikan informasi acara ini ke teman di media sosial sesering mungkin.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success close-btn" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>