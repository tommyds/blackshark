<div id="event" class="event">
	<div class="container">
		<!-- <div class="row">
			<div class="col-md-12">
				<div class="event__content">
					<div data-aos="zoom-in">
						<p class="title_event">
							Black Shark
						</p>
						<p class="casual">
							Casual Meet Up
						</p>
						<p class="desc_meetup col-lg-6 offset-lg-3">
							Black Shark Casual Meetup adalah acara offline meet up yang menghadirkan sarana berkumpul apra mobile gamers untuk berlatih, menajamkan skill, bahkan bertemu dengan gaming influencer. Ini adalah Dojo tempat kamu mengasah kemampuan untuk bertanding di community tournament. Akan ada mini tournament juga di acara meet up sebagai kualifikasi untuk community tournament.
						</p>

						<button type="button" class="btn btn-join btn-lg" data-toggle="modal" data-target="#join-meetup">Daftar Meet Up</button>
					</div>
				</div>
					
			</div>
		</div> -->
		<div class="row">
			<div class="col-md-12">
				<div class="tournament__content">
					<div data-aos="zoom-in">
						<p class="title_tournament">
							Black Shark 
						</p>
						<p class="community_tournament">
							Mini Tournament
						</p>
						<p class="desc_tournament col-lg-6 offset-lg-3">
							Black Shark Mini Tournament adalah tounament untuk para rookie player agar bisa bersinar di ecosystem esports. Kami mengumpulkan 32 team untuk setiap kompetisi (MLBB) dalam single elimination tournament.
						</p>
						<br>
						<p class="desc_tournament col-lg-6 offset-lg-3">Hadiah 1: Rp5.000.000 + 1 buah smartphone terbaru Black Shark</p>
						<p class="desc_tournament col-lg-6 offset-lg-3">Hadiah 2: Rp2.000.000</p>
						<p class="desc_tournament col-lg-6 offset-lg-3">Hadiah 3: Rp1.000.000</p>
						<br><br>
						<p class="desc_tournament col-lg-6 offset-lg-3">Hadiah 1 smartphone terbaru Black Shark untuk MVP (best player)</p>
						<p class="desc_tournament col-lg-6 offset-lg-3">Hadiah 1 smartphone terbaru Black Shark untuk Most Favorite Player</p>
						<button type="button" class="btn btn-join-green btn-lg" data-toggle="modal" data-target="#comm_tournament">Lihat Jadwal</button>
					</div>
				</div>
				
			</div>
		</div>
		<div data-aos="zoom-in">
			<div class="row rules">
				<div class="col-md-4">
					<div class="battleground">
						<!-- <img class="ml" src="{{asset('../images/mobile-legend.png')}}">
						<button type="button" class="btn btn-rules-green btn-lg" data-toggle="modal" data-target="#mobile-legend-game">Lihat Aturan</button> -->
					</div>
				</div>
				<div class="col-md-4">
					<div class="mobile-legend">
						<img class="ml" src="{{asset('../images/mobile-legend.png')}}">
						<button type="button" class="btn btn-rules-green btn-lg" data-toggle="modal" data-target="#mobile-legend-game">Lihat Aturan</button>
					</div>
				</div>
				<div class="col-md-4">
					<div class="cod">
						<!-- <img src="{{asset('../images/cod.png')}}" width="267px">
						<a href="#tnc" type="button" class="btn btn-rules-green btn-lg">Segera</a> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="mobile-legend-game" tabindex="-1" role="dialog" aria-labelledby="mobile-legend-game-tittle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content modal-rules">
      <div class="modal-header">
        <h5 class="modal-title rules-title" id="battle-tittle">Rules Mobile Legend</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-rules">
       		<strong>Registration Rules - <a href="#contact" data-dismiss="modal"> lihat aturan</a> 
			<br><br>
			<p style="font-size: 20px;">Competition Rules:</p>
			</strong><br>
			<strong>1. Peraturan Umum</strong>
			<p>&nbsp;</p>
			<div class="nomor-1">
				<p><strong>1.1 Peserta</strong></p>
				<div class="isi-nomor-1">
					<p><span style="font-weight: 400;">- Peserta adalah individu perorangan yang telah mendaftar dalam form registrasi <a href="#">(link pendaftaran)</a></span></p>

					<p><span style="font-weight: 400;">- Setelah peraturan ini dikirimkan pada peserta maka panitia tidak lagi menerima perubahaan untuk peserta yang telah terdaftar</span></p>

					<p><span style="font-weight: 400;">- Peserta tidak boleh diwakilkan oleh siapapun untuk mengikuti turnamen ini apapun alasannya. Jika melanggar maka akan dianggap tidak hadir</span></p>

					<p><span style="font-weight: 400;">- Peserta harus bersedia dan menerima jam dan tanggal main yang sudah ditentukan oleh panitia</span></p>

					<p><span style="font-weight: 400;">- Untuk pertandingan offline, panitia tidak menyediakan akomodasi</span></p>

					<p><span style="font-weight: 400;">- Peserta wajib untuk mengikuti semua peraturan yang disediakan oleh panitia</span></p>

					<p><span style="font-weight: 400;">- Panitia berhak mengubah peraturan dan memutuskan sesuatu di luar peraturan yang ada</span></p>

					<p><span style="font-weight: 400;">- Dengan mendaftar para peserta dianggap mengerti, paham dan bersedia untuk mematuhi semua peraturan yang ada</span></p>

					<p><span style="font-weight: 400;">- Setiap tim wajib memiliki minimal 5 pemain utama ( boleh memiliki 1 cadangan)</span></p>

					<p><span style="font-weight: 400;">- Minimum rank: EPIC</span></p>

					<p><span style="font-weight: 400;">- Tidak diperbolehkan MULTISLOT atau MULTITEAM.</span></p>
					<p><br /></p>
				</div>
			</div>
			<div class="nomor-1">
				<p><strong>1.2 Fair Play</strong></p>
				<div class="isi-nomor-1">
					<p><span style="font-weight: 400;">- Fair play sangat dianjurkan dan dijunjung tinggi demi terciptanya turnamen yang kondusif</span></p>

					<p><span style="font-weight: 400;">- Dilarang menggunakan program ilegal, bug, match fixing, dan tindak kecurangan lainnya</span></p>
				</div>
			</div>
			<br>
			<div class="nomor-1">
				<p><strong>1.3 Diskualifikasi dan WO</strong></p>
				<div class="isi-nomor-1">
					<p><span style="font-weight: 400;">- Peserta yang tidak bisa hadir setelah 15 menit dari jadwal yang sudah ditentukan akan dianggap gugur atau Walk Out</span></p>

					<p><span style="font-weight: 400;">- Peserta yang mencoba atau melakukan kecurangan dan melanggar peraturan dalam bentuk apapun akan didiskualifikasi</span></p>

					<p><span style="font-weight: 400;">- Peserta yang dengan sengaja melakukan toxic, flaming, menyinggung ras, agama, etnis dan hal pribadi lainnya akan kami diskualifikasi</span></p>

					<p><span style="font-weight: 400;">- Peserta yang dengan sengaja melakukan/menggunakan bug, glitch, atau error pada game yang menguntungkan pengguna akan kami diskualifikasi</span></p>
				</div>
			</div>
			<div class="nomor-1">
				<p><strong><br /></strong><strong>1.4 Sebelum Pertandingan</strong></p>
				<div class="isi-nomor-1">
					<p><span style="font-weight: 400;">- Seluruh peserta diwajibkan siap 30 menit sebelum pertandingan dimulai.</span></p>

					<p><span style="font-weight: 400;">- Peserta wajib hadir sesuai jadwal yang ditentukan dan sudah siap untuk bertanding sesuai dengan jam yang ditentukan juga</span></p>

					<p><span style="font-weight: 400;">- Sebelum memulai turnamen, peserta wajib absen/check in ke panitia</span></p>

					<p><span style="font-weight: 400;">- Jika setelah 15 menit terlambat dari jadwal yang sudah tertera maka akan kami anggap tidak hadir.</span></p>
				</div>
			</div>
			<br>
			<div class="nomor-1">
				<p><strong><em>1.5 Saat Pertandingan</em></strong></p>
				<div class="isi-nomor-1">
					<p><span style="font-weight: 400;">- Koneksi yang menggunakan layanan internet pribadi peserta yang bermasalah di luar tanggung jawab panitia dan menjadi tanggung jawab masing masing peserta.&nbsp;</span></p>
					<p><span style="font-weight: 400;">- Kedua tim melakukan kesepakatan waktu tanding sesuai dengan waktu yang telah dianjurkan (untuk online qualifier).&nbsp;</span></p>

					<p><span style="font-weight: 400;">- Apabila tidak ada kesepakatan waktu, maka match harus dilangsungkan pada Default Time yang telah tertera.&nbsp;</span></p>

					<p><span style="font-weight: 400;">- Toleransi keterlambatan dari kesepakatan waktu tanding maupun default time adalah 15 menit.&nbsp;</span></p>

					<p><span style="font-weight: 400;">- Seluruh peserta menggunakan akun ID masing-masing. Keterlambatan sangat tidak ditoleransi oleh pihak penyelenggara, dan menyebabkan diskualifikasi.&nbsp;</span></p>

					<p><span style="font-weight: 400;">- Apabila sudah memasuki waktu pertandingan (yang telah ditentukan dan diberitahukan sebelumnya), namun anggota dari salah satu tim/kedua tim masih belum lengkap (kurang dari 5 orang), maka pertandingan akan tetap dilanjutkan.&nbsp;</span></p>

					<p><span style="font-weight: 400;">- Jumlah minimum anggota tim agar pertandingan bisa dimulai adalah 4 orang (Jumlah minimum masing-masing tim), apabila ada salah satu tim yang jumlahnya kurang dari 4 orang, maka tim tersebut akan dianggap kalah pada pertandingan tersebut.&nbsp;</span></p>

					<p><span style="font-weight: 400;">- Kesalahan/kelalaian yang terjadi saat pertandingan, yang diakibatkan pribadi dari player/tim bukan tanggung jawab penyelenggara, dan pertandingan akan tetap dilanjutkan.&nbsp;</span></p>

					<p><span style="font-weight: 400;">- Batas maksimal pause adalah 3 menit untuk masing-masing tim pertandingan wajib dilanjutkan kembali apabila melebihi batas maksimal pause.&nbsp;</span></p>

					<p><span style="font-weight: 400;">- Segala bentuk cheat, hack, dan eksploitas tidak ditolerir, apabila salah satu tim melanggar maka game akan segera dihentikan dan tim yang melanggar didiskualifikasi.&nbsp;</span></p>

					<p><span style="font-weight: 400;">- Pemenang wajib melakukan screenshot scoreboard akhir dan melaporkan di group WA, jika tidak ada screenshot yang dilaporkan, maka kedua tim di anggap gugur di match tersebut, dan tidak ada pemenang yang melaju ke babak selanjutnya.</span><span style="font-weight: 400;"></span></p>
					<p><span style="font-weight: 400;">- Dilarang keras melakukan tindakan toxic/flaming/kecurangan secara sengaja maupun tidak disengaja dalam bentuk apapun selama pertandingan berlangsung. Peserta yang melakukan hal-hal di atas akan terkena diskualifikasi oleh panitia.</span></p>

					<p><span style="font-weight: 400;">- Jika pertandingan mengalami error, bug atau glitch harap melaporkan hal tersebut ke panitia terlebih dahulu. Peserta dilarang memutuskan tindak lanjut terkait masalah di atas. Hanya panitia yang berhak memutuskan.</span></p>

					<p><span style="font-weight: 400;">- Permainan akan diulang jika salah satu pemain mengalami error/DC&nbsp; setelah 5 menit pertandingan berlangsung jika tidak ada damage yang terjadi.</span><span style="font-weight: 400;"><br /><br /></span></p>
				</div>
			</div>
			
			<strong>2. Tanggal dan tempat</strong>
			<div class="nomor-1">
				<p><span style="font-weight: 400;">Online - 28, 29, 30 Agustus 2020</span></p>
				<br>
			</div>
			<strong>3. Hadiah</strong><strong><br />
			<div class="nomor-1">
				</strong><span style="font-weight: 400;">Total hadiah 8.000.000 </span>
				<p><span style="font-weight: 400;">Smartphone Black Shark terbaru</span></p>
				<br>
			</div>
			<strong>4. Format Turnamen</strong>
			<div class="nomor-1">
				<p><span style="font-weight: 400;">Mode: Custom - Draft pick 5 v 5</span></p>
				<br>
				<p><span style="font-weight: 400;">Mini Tournament&nbsp;</span></p>
				
				<p><span style="font-weight: 400;">Online qualifier: Pool - BO 1</span></p><br>

				<p><span style="font-weight: 400;">Grand Final: Bo3</span></p><br>

				<p><span style="font-weight: 400;">Skin: ON</span></p>
				<p><span style="font-weight: 400;">Penentuan pick: Toss coin untuk menentukan first pick/first ban.</span></p><br>
				<p><span style="font-weight: 400;">Kondisi menang: </span></p>
				<p><span style="font-weight: 400;">- Salah satu dari base kedua tim hancur.</span></p>
				<p><span style="font-weight: 400;">- Salah satu tim menyatakan surrender.</span></p><br>

				<p><span style="font-weight: 400;">Hero yang dilarang digunakan: Hero yang baru dirilis kurang dalam waktu 2 minggu tidak boleh digunakan.</span></p>
				<p><br /></p>
			</div>
			<strong>5. Komplain</strong>
			<div class="nomor-1">
				<p style="font-weight: 400;"><span style="font-weight: 400;">-Lingkupan komplain : segala situasi yang mempengaruhi keadilan dalam permainan.</span><span style="font-weight: 400;"><br /><br /></span></p>
				<p style="font-weight: 400;"><span style="font-weight: 400;">-Syarat komplain yang harus dipenuhi :</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Materi yang disiapkan team yang komplain setidaknya satu dari video, screenshot dalam permainan, screenshot riwayat chat, foto dan materi lainnya, tidak terbatas pada materi di atas saja. Menjelaskan tim/pemain mana yang dikomplain, menyampaikan detail dan analisa, serta bersedia membantu panitia saat diperlukan untuk menentukan apa komplain tersebut benar melanggar peraturan.</span><span style="font-weight: 400;"><br /><br /></span></p>
				<p style="font-weight: 400;"><span style="font-weight: 400;">-Periode complain:</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Komplain maksimal diajukan dalam 3 jam setelah pertandingan berakhir, jika periode lewat maka komplain tidak diterima.</span><span style="font-weight: 400;"><br /><br /></span></p>
				<p style="font-weight: 400;"><span style="font-weight: 400;">-Cara penyelesaian</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Melalui analisa bukti (pemutaran ulang rekaman, pertimbangan screenshot, dll), jika situasi menunjukkan bahwa ada kecurangan, maka akan ditindak sesuai dengan hukum yang berlaku dan akan dipublikasikan; Hasil pertandingan akan dikoreksi sesuai aturan, jika tidak bisa dikoreksi akan dipertimbangkan tanding ulang.</span></p>
			</div>
			<br>
			<strong>6. Force Majeure</strong><strong></strong>
			<div class="nomor-1">
				<span style="font-weight: 400;"><br /></span><strong><em>Force majeure</em></strong><span style="font-weight: 400;"> yang berarti "kekuatan yang lebih besar" adalah suatu kejadian yang terjadi di luar kemampuan </span><span style="font-weight: 400;">manusia</span><span style="font-weight: 400;"> dan tidak dapat dihindarkan sehingga suatu kegiatan tidak dapat dilaksanakan atau tidak dapat dilaksanakan sebagaimana mestinya.</span>
				<p><span style="font-weight: 400;">Yang termasuk kategori keadaan kahar adalah </span><span style="font-weight: 400;">peperangan</span><span style="font-weight: 400;">, </span><span style="font-weight: 400;">kerusuhan</span><span style="font-weight: 400;">, </span><span style="font-weight: 400;">revolusi</span></a><span style="font-weight: 400;">, </span><span style="font-weight: 400;">bencana alam</span><span style="font-weight: 400;">, </span><span style="font-weight: 400;">pemogokan</span><span style="font-weight: 400;">, </span><span style="font-weight: 400;">kebakaran</span><span style="font-weight: 400;">, dan bencana lainnya yang harus dinyatakan oleh pejabat/instansi yang berwenang.</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Panitia tidak bertanggung jawab jika salah satu dari tim terdampak kerugian dari contoh-contoh kejadian di atas seperti masalah koneksi para pemain juga termasuk.</span></p>
			</div>
			<br>

			<strong> Lainnya</strong>
			<div class="nomor-1">
				<p><span style="font-weight: 400;">Panitia berhak menambahkan, mengubah, peraturan yang ada demi menjaga ketertiban berlangsungnya turnamen. Semua keputusan dari panitia yang bertugas adalah mutlak dan tidak bisa diganggu gugat.</span></p>
			</div>
			<br>
			<a href="http://bit.ly/BSclan" type="button" class="btn btn-join btn-lg">Daftar Sekarang</a>
			<br>
			<br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success close" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="battle" tabindex="-1" role="dialog" aria-labelledby="battle-tittle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content modal-rules">
      <div class="modal-header">
        <h5 class="modal-title rules-title" id="battle-tittle">Rules Battleground</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-rules">
    	Segera
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success close" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="cod-game" tabindex="-1" role="dialog" aria-labelledby="cod-game-tittle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content modal-rules">
      <div class="modal-header">
        <h5 class="modal-title rules-title" id="battle-tittle">Rules Call Of Duty</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-rules">
       	Segera
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success close" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="comm_tournament" tabindex="-1" role="dialog" aria-labelledby="comm_tournament" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content modal-rules">
      <div class="modal-header">
        <h5 class="modal-title rules-title" id="battle-tittle">Mini Tournament</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-rules">
       	<p><span style="color: #fff;">Black Shark Clan Mini Tournament adalah acara kompetisi online untuk para rookie atau tim amatir agar bisa bersinar di game MLBB.</span></p><br>
		<p><span style="color: #fff;">Ini adalah ajang pembuktian bagi kalian tim amatir terbaik MLBB. Kamu akan bertanding untuk menjadi yang terbaik dan mendapatkan hadiah total 8 juta rupiah serta smartphone Black Shark terbaru.</span></p><br>
		<p><span style="color: #fff;">Di acara ini kamu juga bisa mengenal berbagai teknologi terbaru dari BlackShark yang menghadirkan smartphone gaming kelas atas.</span></p><br>
		<p><span style="color: #fff;">Persiapkan tim terbaik kamu, daftar dan lengkapi persyaratan yang ada. Berkenalan dengan teknologi Black Shark dan nikmati serunya bermain game dengan perangkat smartphone gaming terbaik.</span></p><br>
		<p><span style="color: #fff;">Acara akan dilangsungkan secara online tanggal 28, 29, 30 Agustus 2020.</span></p><br>
		<p><span style="color: #fff;">Tournament guidelines: Lihat pertaturan <a href="#contact" data-dismiss="modal">di sini</a>.</span></p><br><br>
		<p><span style="color: #fff;">Contact person: Fauzan (0822-1759-9096) email: fauzhan@hybrid.co.id or admin@hybrid.co.id .</span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success close" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="join-meetup" tabindex="-1" role="dialog" aria-labelledby="join-meetup" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content modal-rules">
      <div class="modal-header">
        <h5 class="modal-title rules-title" id="battle-tittle">Daftar Meet Up</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-rules">
       	<p><span style="font-weight: 400;">Black Shark Clan Casual Meetup adalah acara meetup offline untuk para mobile gamers berkumpul, berlatih dan meningkatkan skill serta bertemu dengan game influencer.&nbsp;</span></p><br>
		<p><span style="font-weight: 400;">Ini adalah dojo untuk menyiapkan diri kamu mengikuti community turnamen. Akan ada mini tournamen juga di acara meetup untuk kualifikasi mengikuti community tournament.</span><span style="font-weight: 400;">.</span></p><br>
		<p><span style="font-weight: 400;">Mini tournament hanya akan bisa diikuti oleh peserta yang hadir ke acara gathering. Akan ada hadiah 1 juta rupiah, serta kamu juga bisa mendapatkan goodie bag. Yang tidak kalah penting, kamu juga bisa mencoba perangkat terbaru dari Black Shark.</span></p><br>
		<p><span style="font-weight: 400;">Merchandise spesial juga akan tersedia untuk para peserta yang hadir selain dari makanan ringan selama acara.&nbsp;</span></p>
		<p><span style="font-weight: 400;">Proses registrasi akan dimulai dari tanggal 8 Maret - 3 April 2020 secara online lewat tautan ini. Acara terbuka secara gratis.&nbsp;</span></p>
		<p><span style="font-weight: 400;">Casual meetup untuk Black Shark Clan akan diadakan pada tanggal 11 April 2020, mulai dari pukul 1 siang sampai selesai.</span></p>
		<p><span style="font-weight: 400;">Acara akan dilakukan di Hybrid Dojo @HybridIDN HQ Jl. Kemang Selatan 1D No.2</span></p><br>
		<p><span style="font-weight: 400;">Jangan lupa untuk membaca secara lengkap persyaratan yang ada lewat tautan ini. Ikuti instruksi yang tersedia, submit jawaban dan tunggu. Kami akan mengontak Anda jika terpilih untuk menghadiri acara.&nbsp;</span></p><br><br>
		<p><span style="font-weight: 400;">Goodluck!</span></p>
		<p><span style="font-weight: 400;">Mini tournament guidelines: Lihat peraturan <a href="#contact" data-dismiss="modal">disini.</a></span></p>
		<p><br /><span style="font-weight: 400;">Contact person: Fauzan (0822-1759-9096) email: </span><a href="mailto:fauzhan@hybrid.co.id"><span style="font-weight: 400;">fauzhan@hybrid.co.id</span></a><span style="font-weight: 400;"> or </span><a href="mailto:admin@hybrid.co.id"><span style="font-weight: 400;">admin@hybrid.co.id</span></a><span style="font-weight: 400;">.</span></p>
       	<a href="http://bit.ly/BSclan" type="button" class="btn btn-join btn-lg">Daftar Meet Up</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success close" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>