<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta name="csrf-token" content="{{csrf_token()}}">
	<meta property="og:url"           content="https://blackshark.hybrid.co.id"/>
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="Blackshark Community" />
	<meta property="og:description"   content="Black Shark Casual Meetup is an offline meetup where mobile gamers gather, practice, spar, and even meet with the gaming influencers. This is your dojo for preparing yourself to the Community Tournament. There will also be a mini tournament here for qualification phase of Community Tournament."/>
	<meta property="og:image"         content="{{asset('../images/Group12.png')}}" />
	<title>Black Shark Clan</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<link rel="icon" type="image/png" href="/favicon.png" />
  
</head>
<body>
	
	@include('layouts.header')
	
	@include('page.home')
	@include('page.tnc')
	@include('page.contact')
	@include('page.about_us')
	@include('page.event')
	
	@include('page.ads')
	
	@include('layouts.footer')
	
	<script
  		src="https://code.jquery.com/jquery-3.4.1.min.js"
  		integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  		crossorigin="anonymous">
  	</script>
  	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  	<script src="{{asset('js/app.js')}}"></script>
  	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  	<script>
  		AOS.init({
  			duration: 1000
  		});
	</script>
  	<!-- Modal -->
</body>
</html>