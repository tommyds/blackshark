<div id="footer" class="footer">
	<p class="follow-us">Follow us on:</p>
	<a href="https://www.facebook.com/BlackSharkGlobal/" class="btn btn-pop-up"><img src="{{asset('../images/facebook.svg')}}"></a>
	<!-- <a href="https://twitter.com/blckshrk_global?s=20" class="btn btn-pop-up"><img src="{{asset('../images/twitter.svg')}}"></a> -->
	<a href="https://www.instagram.com/blackshark.idn/" class="btn btn-pop-up"><img src="{{asset('../images/instagram.svg')}}"></a>
	<p class="follow-us">
		Supported by:
	</p>
	<p class="follow-us">
		<a href="https://global.blackshark.com/" style="margin-right: 5px"><img src="{{asset('../images/logoshark1.png')}}" width="145" height="38"></a><a href="https://hybrid.co.id/" style="margin-right: 5px"><img src="{{asset('../images/WHITE.png')}}"></a><a href="http://bit.ly/392sVN9"><img src="{{asset('../images/erafon.png')}}" width="145" height="25" style="margin-bottom: 7px;"></a>
		<p class="copyright">Copyright©2020.</p>	
	</p>
</div>